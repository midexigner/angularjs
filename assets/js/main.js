"use strict";
// -------------------------------------------------------------
    // bootstrap dropdown hover menu setup
    // -------------------------------------------------------------

$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(400);
      $('span', this).toggleClass("caret caret-up");
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(400);
$('span', this).toggleClass("caret caret-up");
});
$('.dropdown-toggle').removeAttr('data-toggle');

// -------------------------------------------------------------
    // WOW setup
    // -------------------------------------------------------------

jQuery(function ($) {
      var wow = new WOW({
      mobile:       false
      });
      wow.init();
    }());

// -------------------------------------------------------------
    // date  setup
    // -------------------------------------------------------------
   // var d = new Date();
    //var n = d.getFullYear();
    //document.getElementById("date-year").innerHTML = n;
   // -------------------------------------------------------------
    // Modal Animation  setup
    //
    $(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
// -------------------------------------------------------------
    // Filter Button  setup
    //
  $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');

        }
    });

    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});