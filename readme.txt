
=> Form Classes
-> ng-pristine  - when form/input not used yet
-> ng-dirty - when form/input has been used

-> ng-untouched - when input has not been touched
-> ng-touched - when input has been touched 

-> ng-valid - when a form field is valid
-> ng-invalid - when a form field is not valid


=> Form Properties

-> ng-pristine  -   $pristine
-> ng-dirty     -   $dirty
-> ng-valid     -   $valid
-> ng-invalid   -   $invalid