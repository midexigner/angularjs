app = angular.module('app',['ngRoute','ngAnimate']);

app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){

$locationProvider.html5Mode(true);

	$routeProvider
	.when('/home',{
		templateUrl:'views/home.html',
		controller:'MainController'
	})
	.when('/directory',{
		templateUrl:'views/list.html',
		controller:'MainController'
	})
	.when('/contact',{
		templateUrl:'views/contact.html',
		controller:'ContactController'
	})
	.when('/contact-success',{
		templateUrl:'views/contact-success.html',
		controller:'ContactController'
	})
	.otherwise({
		redirectTo:'/home'
	})
	
}]);

app.run(function(){
	
	
});
app.directive('randomNinja',[function(){
	return {
		restrict:'E',
		scope:{
			ninjas:'=',
			title:'='
		},
		//template:'{{title}}<img ng-src="{{ninjas[random].thumb}}"/>',
		templateUrl:'views/random.html',
		transclude:true,
		replace:true,
		controller:function($scope){
		$scope.random = Math.floor(Math.random() * 4);
		}
	};
}]);
app.controller('MainController',['$scope','$http',function($scope,$http){
$scope.message = 'Hello ';	
$scope.remove = function(app){
var remove = $scope.ninjas.indexOf(app);
$scope.ninjas.splice(remove,1);
}
$scope.addNinja = function(){
	$scope.ninjas.push({
		name: $scope.newninja.name,
		belt: $scope.newninja.belt,
		rate: parseInt($scope.newninja.rate),
		available:true
	});

$scope.newninja.name ='';
$scope.newninja.belt ='';
$scope.newninja.rate ='';
};
$scope.removeAll = function(){
	$scope.ninjas = [];
}
$http.get('data/ninjas.json').success(function(data){
$scope.newninjas = data;
//alert($scope.newninjas);
});
$scope.ninjas = [
{
	name:'yoshi',
	belt:'green',
	rate:20,
	available:true,
	thumb:'https://via.placeholder.com/150x150'
},{
	name:'crystal',
	belt:'orange',
	rate:60,
	available:true,
	thumb:'https://via.placeholder.com/150x150'
},{
	name:'ryu',
	belt:'yellow',
	rate:80,
	available:false,
	thumb:'https://via.placeholder.com/150x150'
},{
	name:'shaun',
	belt:'brown',
	rate:90,
	available:true,
	thumb:'https://via.placeholder.com/150x150'
}
];	
console.log(angular.toJson($scope.ninjas));
}]);

app.controller('ContactController',['$scope','$location', function($scope,$location){

	$scope.sendMessage = function(){
		$location.path('contact-success')
	}

}]);